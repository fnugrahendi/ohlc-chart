// const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: './src/index.js',
    plugins: [
        new MiniCssExtractPlugin({
          filename: "style.css"
        })
    ],
    module: {
        rules: [
            {
                test: /\.html$/,
                loader: 'file-loader?name=[name].[ext]'
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: [
                  {
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                      // you can specify a publicPath here
                      // by default it use publicPath in webpackOptions.output
                    //   publicPath: '../'
                    }
                  },
                  "css-loader"
                ]
            }
        ]
    },
    resolve: {
    extensions: ['*', '.js', '.jsx']
    },
    output: {
      path: __dirname + '/public/',
      publicPath: '/public/',
      filename: 'bundle.js'
    },
    devServer: {
      contentBase: './public'
    }
};