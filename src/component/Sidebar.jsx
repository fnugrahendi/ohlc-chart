import React from 'react';
import './Sidebar.css';

export default class Sidebar extends React.Component {
    render() {
        const { availableStocks, selectedStock } = this.props;
        return (
            <div className={'sidebar'}>
                {
                    Object.keys(this.props.availableStocks).map(key => {
                        return <div className={`menu${selectedStock == availableStocks[key] ? ' selected' : ''}`} onClick={() => this.props.onSelectStock(availableStocks[key])}>
                            {availableStocks[key]}</div>
                    })
                }
            </div>
        );
    }
}