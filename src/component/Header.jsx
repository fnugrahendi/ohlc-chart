import React from 'react';

const headerStyle = {
    position: 'fixed',
    top: '0',
    left: '0',
    width: '100%',
    height: '60px',
    backgroundColor: 'maroon',
    color: 'white'
}

const headerTextStyle = {
    margin: '15px 15px',
    fontSize: '26px'
}

export default class Header extends React.Component {
    render() {
        return (
            <div style={headerStyle} className={'header'}>
                <p style={headerTextStyle}>OHLC Header</p>
            </div>
        );
    }
}