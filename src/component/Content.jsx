import React from 'react';
import OHLCChart from './OHLCChart';

const contentStyle = {
    position: 'fixed',
    left: '160px',
    top: '60px',
    width: 'calc(100% - 160px)',
    height: 'calc(100% - 120px)',
    overflowY: 'auto'
}

const timeSeries = {
    DAILY: 'Time Series (Daily)'
}

export default class Content extends React.Component {
    render() {
        const stockValues = this.props.apiRequestValue[timeSeries.DAILY] ? this.props.apiRequestValue[timeSeries.DAILY] : {};
        console.log('stockValues: ', stockValues);
        let chartData = [];
        for (let dataId = 0; dataId < Object.keys(stockValues).length; dataId++) {
            const currentDate = Object.keys(stockValues)[dataId];
            chartData.push(
                {
                    x: new Date(currentDate),
                    y: [
                        stockValues[currentDate]['1. open'],
                        stockValues[currentDate]['2. high'],
                        stockValues[currentDate]['3. low'],
                        stockValues[currentDate]['4. close'],
                    ]
                }
            );
        }
        return (
            <div style={contentStyle} className={'content'}>
                <OHLCChart data={chartData} />
                {/* <div style={{ display: 'table', height: '100%', margin: '30px 25px' }}>
                    <div style={{ display: 'table-row' }}>
                        <div style={{ display: 'table-cell', padding: '10px 10px', fontWeight: 'bold' }}>
                            OPEN
                        </div>
                        <div style={{ display: 'table-cell', padding: '10px 10px', fontWeight: 'bold' }}>
                            HIGH
                        </div>
                        <div style={{ display: 'table-cell', padding: '10px 10px', fontWeight: 'bold' }}>
                            LOW
                        </div>
                        <div style={{ display: 'table-cell', padding: '10px 10px', fontWeight: 'bold' }}>
                            CLOSE
                        </div>
                    </div>
                    {
                        Object.keys(stockValues).map((key) => {
                            return <div style={{ display: 'table-row' }}>
                                <div style={{ display: 'table-cell', padding: '10px 10px' }}>
                                    {key}
                                </div>
                                {
                                    Object.keys(stockValues[key]).map((type) => {
                                        return <div style={{display: 'table-cell'}}>
                                            {stockValues[key][type]}
                                        </div>
                                    })
                                }
                            </div>
                        })
                    }
                </div> */}
            </div>
        );
    }
}