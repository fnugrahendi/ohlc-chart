import React from 'react';

const footerStyle = {
    position: 'fixed',
    bottom: '0',
    left: '0',
    width: '100%',
    height: '60px',
    backgroundColor: 'black',
    color: 'white'
}

const footerTextStyle = {
    margin: '15px 15px',
    fontSize: '26px'
}

export default class Footer extends React.Component {
    render() {
        return (
            <div style={footerStyle} className={'footer'}>
                <p style={footerTextStyle}>OHLC Footer</p>
            </div>
        );
    }
}