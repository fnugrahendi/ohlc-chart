import React from 'react';

const CanvasJSReact  = require('./../lib/canvasjs.react');
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default class OHLCChart extends React.Component {
	render() {
        console.log('data: ', this.props.data);
		const options = {
			animationEnabled: true,
			exportEnabled: true,
			theme: "light2",
			exportFileName: "OHLC Chart",
			title:{
				text: "Chart"
			},
			axisX: {
				interval:1,
				intervalType: "month",
				valueFormatString: "MMM"
			},
			axisY: {
				includeZero:false,
				prefix: "$",
				title: "Price (in USD)"
			},
			data: [{
				type: "ohlc",
				yValueFormatString: "$###0.00",
				xValueFormatString: "MMM YYYY",
				dataPoints: this.props.data
			}]
		}
		return (
		<div>
			<CanvasJSChart options = {options}
				onRef={ref => this.chart = ref}
			/>
		</div>
		);
	}
}