import React from 'react';
import axios from 'axios';
import Header from './component/Header';
import Footer from './component/Footer';
import Sidebar from './component/Sidebar';
import Content from './component/Content';

const availableStocks = {
    MSFT: 'MSFT',
    AAPL: 'AAPL',
    INTC: 'INTC',
    NFLX: 'NFLX',
    ORCL: 'ORCL',
    CMCSA: 'CMCSA',
    GOOG: 'GOOG',
    LUV: 'LUV',
    HOG: 'HOG',
    GOOGL: 'GOOGL',
    AMZN: 'AMZN'
};

const apiKey = 'LB8SYWGFMRVTC8M9';

export default class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            selectedStock: '',
            apiRequestValue: {}
        }
        this.onSelectStock = this.onSelectStock.bind(this);
        this.requestStockUpdate = this.requestStockUpdate.bind(this);
        this.updateValue = this.updateValue.bind(this);
    }
    onSelectStock(stock){
        this.setState({ selectedStock: stock });
        this.requestStockUpdate(stock);
    }
    updateValue(value){
        this.setState({ apiRequestValue: value });
    }
    requestStockUpdate(stock){
        const URL = 'https://www.alphavantage.co/';
        const requestURL = `${URL}query?function=TIME_SERIES_DAILY&symbol=${stock}&apikey=${apiKey}`;
        console.log('request URL', requestURL);
        axios.get(requestURL)
            .then(response => {
                // handle success
                this.updateValue(response.data);
                console.log('response', response);
            })
            .catch(error => {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }
    render() {
        return (
            <div>
                <Header />
                <Sidebar
                    availableStocks={availableStocks}
                    selectedStock={this.state.selectedStock}
                    onSelectStock={this.onSelectStock}
                />
                <Content
                    apiRequestValue={this.state.apiRequestValue}
                />
                <Footer />
            </div>
        );
    }
}