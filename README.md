# OHLC Chart

OHLC Chart in ReactJS

**Dependencies:**
-. NodeJs

**How to run:**
1. clone/download source
2. from its root folder, run ```npm install```
3. run ```npm run build``` and then ```npm start```
4. go to http://localhost:8080 from your browser